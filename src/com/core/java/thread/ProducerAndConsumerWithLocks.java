package com.core.java.thread;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class MyWorker{
	
	private static Lock lock = new ReentrantLock();
	private static Condition condition = lock.newCondition();
	
	public void producer() throws InterruptedException{
		lock.lock();
		try {
			System.out.println("Producer method ... ");
			condition.await();
			System.out.println("Producer again ..");
		}finally {
			lock.unlock();
		}
		
	}
	
	public void consumer() throws InterruptedException{
		lock.lock();
		try {
			Thread.sleep(2000);
			System.out.println("Consumer method ... ");
			condition.signal();
		}finally {
			lock.unlock();
		}
		
	}
}

public class ProducerAndConsumerWithLocks {

	public static void main(String[] args) {

		MyWorker worker = new MyWorker();
		
		Thread t1 = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					worker.producer();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});

		Thread t2 = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					worker.consumer();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		
		t1.start();
		t2.start();
		 
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
}
