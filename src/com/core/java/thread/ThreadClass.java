package com.core.java.thread;

class Executor1 extends Thread {

	@Override
	public void run() {

		for (int i = 0; i < 30; i++) {
			System.out.println("Runner 1: " + i);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

}

class Executor2 extends Thread {

	@Override
	public void run() {

		for (int i = 0; i < 30; i++) {
			System.out.println("Runner 2: " + i);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

}

public class ThreadClass {

	public static void main(String[] args) {

		Executor1 t1 = new Executor1();
		Executor2 t2 = new Executor2();

		t1.start();
		t2.start();
		
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Finished the tasks ...");
	}
}
