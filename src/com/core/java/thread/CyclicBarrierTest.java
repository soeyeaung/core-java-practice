package com.core.java.thread;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class BarrierWorker implements Runnable{

	private int id;
	private Random random;
	private CyclicBarrier cyclicBarrier;
	
	
	
	public BarrierWorker(int id, CyclicBarrier barrier) {
		super();
		this.id = id;
		this.random = new Random();
		this.cyclicBarrier = barrier;
	}

	@Override
	public void run() {
		doWork();
	}

	private void doWork() {
		System.out.println("Thread with ID " + id + " starts the task...");
		try {
			Thread.sleep(random.nextInt(3000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Thread with ID " + id + " finished...");
		
		try {
			cyclicBarrier.await();
			System.out.println("After await ....");
		} catch (InterruptedException | BrokenBarrierException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public String toString() {
		return "" + this.id;
	}
	
}

public class CyclicBarrierTest {

	public static void main(String[] args) {
		
		ExecutorService executorService = Executors.newFixedThreadPool(5);
		CyclicBarrier barrier = new CyclicBarrier(5, new Runnable() {
			
			@Override
			public void run() {
				System.out.println("We are able to train the neutral network...");
			}
		});
		
		for(int i=0; i<5; i++) {
			executorService.execute(new BarrierWorker(i+1, barrier));
		}
		
		executorService.shutdown();
	}
}
