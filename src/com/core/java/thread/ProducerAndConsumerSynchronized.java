package com.core.java.thread;

class MyWorker2{
	
	private static final Object lock = new Object();
	
	public void producer() throws InterruptedException{
		
		synchronized (lock) {
			System.out.println("Producer method ... ");
			lock.wait();
			System.out.println("Producer again ..");
		}
		
	}
	
	public void consumer() throws InterruptedException{
		
		synchronized (lock) {
			System.out.println("Consumer method ... ");
			lock.notify();
		}
		
	}
}

public class ProducerAndConsumerSynchronized {
	
	public static void main(String[] args) {

		MyWorker2 worker = new MyWorker2();
		
		Thread t1 = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					worker.producer();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});

		Thread t2 = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					worker.consumer();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		
		t1.start();
		t2.start();
		 
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
}
