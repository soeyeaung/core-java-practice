package com.core.java.thread;

class Process {

	public void produce() throws InterruptedException {

		synchronized (this) {
			System.out.println("We are in the producer method ... ");
			wait(10000);
			System.out.println("Again in producer ...");
		}
	}

	public void consume() throws InterruptedException {

		Thread.sleep(1000);

		synchronized (this) {
			System.out.println("Now in the consumer method..");
			notify();
		}
	}

}

public class WaitAndNotify {

	public static void main(String[] args) {

		Process process = new Process();

		Thread t1 = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					process.produce();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});

		Thread t2 = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					process.consume();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		
		t1.start();
		t2.start();
		
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
