package com.core.java.thread;


class RunnerOne implements Runnable{
	
	@Override
	public void run() {
		
		for(int i=0; i<= 10; i++) {
			System.out.println("Runner 1: " + i);
		}
		
	}
	
}

class RunnerTwo implements Runnable{
	
	@Override
	public void run() {
		
		for(int i=0; i<= 10; i++) {
			System.out.println("Runner 2: " + i);
		}
		
	}
	
}

public class RunnableThread {

	public static void main(String[] args) {
		
		Thread t1 = new Thread(new RunnerOne());
		Thread t2 = new Thread(new RunnerTwo());
		
		t1.start();
		t2.start();
	}
}
