package com.core.java.thread;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CountDownLatchTest {

	public static void main(String[] args) {
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		CountDownLatch latch = new CountDownLatch(5);
		
		for(int i=0; i<5; i++) {
			executorService.execute(new Features(i+1, latch));
		}
		
		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("All funtions had been worked done!");
		executorService.shutdown();
	}
}

class Features implements Runnable{

	private int id;
	private CountDownLatch countDownLatch;
	
	
	public Features(int id, CountDownLatch countDownLatch) {
		super();
		this.id = id;
		this.countDownLatch = countDownLatch;
	}


	@Override
	public void run() {
		downloadingFiles();
		countDownLatch.countDown();
	}
	
	public void downloadingFiles() {
		System.out.println("Downloading files function starting ...");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
