package com.core.java.thread;

public class SynchronizedBlocks {

	private static int count1 = 0;
	private static int count2 = 0;
	
	private static synchronized void add() {
		++count1;
	}
	
	private static synchronized void addAgain() {
		++count2;
	}
	
	private static void compute() {
		for(int i=0; i < 100; ++i) {
			add();
			addAgain();
		}
		
	}
	
	public static void main(String[] args) {

		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				compute();
			}
		});

		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				compute();
			}
		});

		t1.start();
		t2.start();

		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Count 1: " + count1 + " count 2: " + count2);
	}
}
