package com.core.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * @Author ~ Soe Ye Aung
 * @Merge Sort Algorithm
 * 
 * method mergesort(list l):
		if list.size < 2:
			return l
			
		let middleIndex = l.size / 2
		let leftList = elements between l(0) and l(middleIndex - 1)
		let rightList = elements between l(middleIndex) and l(size – 1)
		let sortedLeft = mergesort(leftList)
		let sortedRight = mergesort(rightList)
		return merge(sortedLeft, sortedRight)
		
	method merge(list l, list r):
		let leftPtr = 0
		let rightPtr = 0
		let toReturn = new list
		
		while (leftPtr < l.size and rightPtr < r.size):
			if(l(leftPtr) < r(rightPtr)):
				toReturn.add(l(leftPtr))
				leftPtr++
			else:
				toReturn.add(r(rightPtr))
				rightPtr++
				
		while(leftPtr < l.size):
			toReturn.add(l(leftPtr))
			leftPtr++
			
		while(rightPtr < r.size):
			toReturn.add(r(rightPtr))
			rightPtr++
			
		return toReturn
 * 
 * 
 */
public class MergeSortAlgorithm {

	public static List<Integer> mergeSort(final List<Integer> numbers){
		
		if(numbers.size() < 2)
			return numbers;
		
		final List<Integer> leftHalf = numbers.subList(0, numbers.size() / 2);
		final List<Integer> rightHalf = numbers.subList(numbers.size() / 2, numbers.size());
		return mergeSort(mergeSort(leftHalf), mergeSort(rightHalf));
	}

	private static List<Integer> mergeSort(List<Integer> left, List<Integer> right) {
		int leftPtr = 0;
		int rightPtr = 0;
		
		final List<Integer> merged = new ArrayList<Integer>(left.size() + right.size());
		
		while(leftPtr < left.size() && rightPtr < right.size()) {
			if(left.get(leftPtr) < right.get(rightPtr)) {
				merged.add(left.get(leftPtr));
				leftPtr++;
			}else {
				merged.add(right.get(rightPtr));
				rightPtr++;
			}
		}
		
		while(leftPtr < left.size()) {
			merged.add(left.get(leftPtr));
			leftPtr++;
		}
		
		while(rightPtr < right.size()) {
			merged.add(right.get(rightPtr));
			rightPtr++;
		}
		
		return merged;
	}
	
	public static void main(String[] args) {
		List<Integer> numbers = Arrays.asList(54,65,32,87,21,34,90);
		List<Integer> sorted = mergeSort(numbers);
		for(Integer number:sorted) {
			System.out.println(number);
		}
	}
}
