package com.core.algorithms;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CustomSort {

	public void customSorting() {
		final List<Integer> numbers = Arrays.asList(5,6,2,7,8,9,1);
		Collections.sort(numbers, new ReverseNumericalOrder());
	}
}
