package com.core.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NotComparable {

	private int i;
	
	protected NotComparable(int i) {
		this.i = i;
	}

	public static void sortNotComparableTest() {
		final List<NotComparable> objects = new ArrayList<>();
		for(int i = 0; i < 10; i++) {
			objects.add(new NotComparable(i));
		}
		
		Arrays.sort(objects.toArray());
		for(int i = 0; i < objects.toArray().length; i++) {
			System.out.println(objects.toArray()[i]);
		}
	}	
	
	public static void main(String[] args) {
		sortNotComparableTest();
	}
}
