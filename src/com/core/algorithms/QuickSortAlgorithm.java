package com.core.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * @Author ~ Soe Ye Aung
 * @Quick Sort Algorithm
 * 
 * method quicksort(list l):
		if l.size < 2:
			return l
			
		let pivot = l(0)
		let lower = new list
		let higher = new list
		for each element e in between l(0) and the end of the list:
			if e < pivot:
				add e to lower
			else 
				add e to higher
				
		let sortedlower = quicksort(lower)
		let sortedhigher = quicksort(higher)
		
		return sortedlower + pivot + sortedhigher
 * 
 */
public class QuickSortAlgorithm {

	public static List<Integer> quickSort(List<Integer> numbers) {
		
		if(numbers.size() < 2) 
			return numbers;
	
		final Integer pivot = numbers.get(0);
		final List<Integer> lower = new ArrayList<Integer>();
		final List<Integer> higher = new ArrayList<Integer>();
		
		for(int i=1; i<numbers.size(); i++) {
			if(numbers.get(i) < pivot) {
				lower.add(numbers.get(i));
			}else {
				higher.add(numbers.get(i));
			}
		}
		
		List<Integer> sorted = quickSort(lower);
		sorted.add(pivot);
		sorted.addAll(quickSort(higher));
		return sorted;
	}
	
	public static void main(String[] args) {
		List<Integer> numbers = Arrays.asList(54,65,32,87,21,34,90);
		List<Integer> sorted = quickSort(numbers);
		for(Integer number:sorted) {
			System.out.println(number);
		}
	}
}
