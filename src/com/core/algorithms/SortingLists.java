package com.core.algorithms;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SortingLists {

	public static void sortInts() {
		final int[] numbers = {-3,-5, 1, 7, 4, -2};
		Arrays.sort(numbers);
		for(int i = 0; i < numbers.length; i++) {
			System.out.println(numbers[i]);
		}
	}
	
	public static void sortObjects() {
		final String[] engChars = {"z", "x", "y", "abc", "zzz", "zazzy"};
		Arrays.sort(engChars);
		for(int i = 0; i < engChars.length; i++) {
			System.out.println(engChars[i]);
		}
	}
	
	public static void sortReverseOrder() {
		List<Integer> numbers = Arrays.asList(456, 765, 125, 794, 784, 435, 123);
		numbers.sort(Comparator.reverseOrder());
	}
	
	public static void main(String[] args) {
		sortInts();
		System.out.println("--------------------------------");
		sortObjects();
	}
}
