package com.core.algorithms;

/*
 * @Author - Soe Ye Aung
 * 
 * for i between 0 and (array length – 2):
		if (array(i + 1) < array(i)):
			switch array(i) and array(i + 1)
		repeat until a complete iteration where no elements are switched.
		
		Following is a simple example with a small list:
		6, 4, 9, 5 -> 4, 6, 9, 5: When i = 0, the numbers 6 and 4 are switched
		4, 6, 9, 5 -> 4, 6, 5, 9: When i = 2, the numbers 9 and 5 are switched
		
		4, 6, 5, 9: The first iteration switched numbers, so iterate again
		4, 6, 5, 9 -> 4, 5, 6, 9: When i = 1, the numbers 6 and 5 are switched
		4, 5, 6, 9: The second iteration swiched numbers, so iterate again
		4, 5, 6, 9: No more numbers to switch, so this is the sorted list.
 */

public class BubbleSortAlgorithm {

	public int[] bubbleSort(int[] unsortedNumbers) {
		int[] sortedNumbers = {};
		
		boolean numbersSwitched;
		do {
			numbersSwitched = false;
			for(int i=0; i < unsortedNumbers.length-1; i++) {
				if(unsortedNumbers[i+1] < unsortedNumbers[i]) {
					int tmp = unsortedNumbers[i+1];
					unsortedNumbers[i+1] = unsortedNumbers[i];
					unsortedNumbers[i] = tmp;
					numbersSwitched = true;
				}
			}
		}while(numbersSwitched);
		
		sortedNumbers = unsortedNumbers;
		return sortedNumbers;
	}
	
	public static void main(String[] args) {
		int[] sortedNumbers = {};
		BubbleSortAlgorithm  bubbleSort = new BubbleSortAlgorithm();
		int[] unsortedNumbers = {5,3,7,9,1,6,4,2};
		sortedNumbers = bubbleSort.bubbleSort(unsortedNumbers);
	}
}
