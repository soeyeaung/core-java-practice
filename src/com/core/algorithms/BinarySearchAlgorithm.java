package com.core.algorithms;

import java.util.Arrays;
import java.util.List;

/*
 * @Author ~ Soe Ye Aung
 * @Binary Search
 * 
 * method binarySearch(list l, element e):
		if l is empty:
			return false
			
		let value = l(l.size / 2)Summary
		if (value == e):
			return true
		if (e < value):
			return binarySearch(elements between l(0) and l(l.size / 2 - 1)
		else:
			return binarySearch(elements between l(l.size / 2 + 1) and l(l.size)
 * 
 * 
 */
public class BinarySearchAlgorithm {

	public static boolean binarySearch(final List<Integer> numbers, final Integer value) {
		
		if(numbers == null || numbers.isEmpty())
			return false;
		
		final Integer comparison = numbers.get(numbers.size()/2);
		
		if(value.equals(comparison))
			return true;
		
		if(value < comparison)
			return binarySearch(numbers.subList(0, numbers.size()/2), value);
		else
			return binarySearch(numbers.subList(numbers.size()/2 + 1, numbers.size()), value);
	}
	
	public static void main(String[] args) {
		boolean found = false;
		List<Integer> sortedNumbers = Arrays.asList(21,32,34,54,65,87,90);
		found = binarySearch(sortedNumbers, 21);
		if(found) {
			System.out.println("Found !");
		}else {
			System.out.println("Sorry! Not Found!");
		}
		
	}
}
