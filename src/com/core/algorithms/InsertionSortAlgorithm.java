package com.core.algorithms;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/*
 * @Author - Soe Ye Aung
 * @Algorithm - Insert Sort Algorithm
 * 
		Given a list l, and a new list nl
		for each element originallistelem in list l:
			for each element newlistelem in list nl:
				if (originallistelem < newlistelem):
					insert originallistelem in nl before newlistelem
				else move to the next element
			if originallistelem has not been inserted:
				insert at end of nl
 */

public class InsertionSortAlgorithm {

	public static int[] insertSort(int[] unsortedElements) {		
		for (int i = 1; i < unsortedElements.length; i++) {
			int tmp = unsortedElements[i];
			int j = i - 1;
			while (j >= 0 && unsortedElements[j] > tmp) {
				unsortedElements[j + 1] = unsortedElements[j];
				j--;
			}
			unsortedElements[j + 1] = tmp;
		}
		int[] sortedElements = unsortedElements;		
		return sortedElements;
	}

	public static List<Integer> insertSort2(final List<Integer> numbers) {
		final List<Integer> sortedList = new LinkedList<>();
		unSortedList: for (Integer number : numbers) {
			for (int i = 0; i < sortedList.size(); i++) {
				if (number < sortedList.get(i)) {
					sortedList.add(i, number);
					continue unSortedList;
				}
			}
			sortedList.add(sortedList.size(), number);
		}
		return sortedList;
	}

	public static void main(String[] args) {
		//int[] unsortedElements = { 45, 76, 12, 34, 8, 32, 9, 1, 55 };
		final List<Integer> unsortedNumbers = Arrays.asList( 45, 76, 12, 34, 8, 32, 9, 1, 55 );
		
		//int[] sortedElements = insertSort(unsortedElements);
		List<Integer> sortedElements = insertSort2(unsortedNumbers);
		for (int i = 0; i < sortedElements.size(); i++) {
			System.out.println(sortedElements.get(i));
		}
	}
}
