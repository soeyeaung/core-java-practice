package com.core.algorithms;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class BubbleSortAlgorithmTest {

	@Test
	public void bubbleSortTest() {
		int[] inputInts = {5,3,7,9,1,6,4,2};
		final int[] expected = {1,2,3,4,5,6,7,9};
		
		BubbleSortAlgorithm bubbleSort = new BubbleSortAlgorithm();
		try {
			bubbleSort.bubbleSort(inputInts);
		}catch(Exception e) {
			e.printStackTrace();
		}
		assertArrayEquals(expected, inputInts);
	}
	
}
