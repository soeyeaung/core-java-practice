package com.core.algorithms;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

public class SortingWithoutAComparableTest {

	@Test
	public void sortNotComparableTest() {
		final List<NotComparable> objects = new ArrayList<>();
		for(int i = 0; i < 10; i++) {
			objects.add(new NotComparable(i));
		}
		
		try {
			Arrays.sort(objects.toArray());
		}catch(Exception e) {
			fail();			
		}
		
		
	}
	
}
