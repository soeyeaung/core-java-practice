package com.core.algorithms;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

class SortingListsTest {

	@Test
	public void testSortInts() {
		final int[] numbers = {-3,-5, 1, 7, 4, -2};
		final int[] expected = {-5, -3, -2, 1, 4, 7};
		Arrays.sort(numbers);
		assertArrayEquals(expected, numbers);
	}
	
	@Test
	public void testSortObjects() {
		final String[] engChars = {"z", "x", "y", "abc", "zzz", "zazzy"};
		final String[] expected = {"abc", "x", "y", "z", "zazzy", "zzz"};
		Arrays.sort(engChars);
		assertArrayEquals(expected, engChars);
	}

}
