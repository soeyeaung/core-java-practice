package com.core.algorithms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class MergeSortAlgorithmTest {

	@Test
	public void mergeSortTest() {
		List<Integer> numbers = Arrays.asList(54,65,32,87,21,34,90);
		List<Integer> expected = Arrays.asList(21,32,34,54,65,87,90);
		List<Integer> sorted = MergeSortAlgorithm.mergeSort(numbers);
		assertEquals(expected, sorted);
	}
}
