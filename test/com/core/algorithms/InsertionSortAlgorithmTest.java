package com.core.algorithms;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class InsertionSortAlgorithmTest {

	@Test
	public void insertSortTest() {
		int[] sortedElements = {};
		int[] unsortedElements = {45,76,12,34,8,32,9,1,55};
		int[] expectedElements = {1,8,9,12,32,34,45,55,76};
		
		sortedElements = InsertionSortAlgorithm.insertSort(unsortedElements);
		assertArrayEquals(expectedElements, sortedElements);
	}
	
	@Test
	public void insertSort2Test() {
		
		List<Integer> unsortedElements = Arrays.asList(45,76,12,34,8,32,9,1,55);
		List<Integer> expectedElements = Arrays.asList(1,8,9,12,32,34,45,55,76);
		
		List<Integer> sortedElements = InsertionSortAlgorithm.insertSort2(unsortedElements);
		assertEquals(expectedElements, sortedElements);
	}
}
