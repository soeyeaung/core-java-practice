package com.core.algorithms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

public class CustomSortTest {

	@Test
	public void customSortingTest() {
		final List<Integer> numbers = Arrays.asList(5,6,2,7,8,9,1);
		final List<Integer> expected = Arrays.asList(9,8,7,6,5,2,1);
		
		Collections.sort(numbers, new ReverseNumericalOrder());
		assertEquals(expected, numbers);
	}
}
